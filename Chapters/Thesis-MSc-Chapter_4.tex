% #############################################################################
% This is Chapter 4
% !TEX root = ../main.tex
% #############################################################################
% Change the Name of the Chapter i the following line
\fancychapter{Implementation}
\cleardoublepage
% The following line allows to ref this chapter
\label{chap:implementation}

\section{Chapter description}
In this chapter, the project implementation will be explained. 
To better understand the extent of the implementation, it first begins by explaining the two main limitations present during the development phase.
Afterwards the training environment used to construct and train the agents will be detailed alongside the reasoning for the choices made.
The next section explains the architecture of the \ac{ANN} that each agent possesses.
The fifth section details how the \ac{UMLAT} handles the training of an \ac{ANN}.
Next how the trained model was introduced into Pic-a-boo.
The final two sections are about the procedures that were performed to test the generated agents.

\section{Limitations}

In the development of this project there were certain factors that limited the scope of the study.

\subsection{Agent training time}

Previously it was referred that the most time-consuming part of using an \ac{ANN} is the training phase.
An \ac{ANN} requires many training iterations to be able to make good predictions.
This project used \ac{UMLAT} to train the agents by reinforcement learning with human players as reference.
To train one agent, one player had to play against a set of agent for 10000 iterations which took around 20 minutes in a simplified environment.
Some improvements were found passing the 10000 mark, but it is difficult to find subjects willing to take more than 20 minutes training an agent.
Supposing that the training time of a clone increases linearly using for instance 100000 steps it would take around 200 minutes to train a clone.
So much time is unreasonable for a one sit training session, several sessions would have to be made which is cumbersome.
For this reason the iterations were limited to 10000 to be easier to manage.

The training time also limits the quantity of tested architectures, finding a good architecture for an \ac{ANN} entails a process of trial and error.
Testing an architecture implies training agents and checking the results which may take around 20 minutes per architecture.
\ac{ANN} had many possible configurations, for instance number of layers, number of neurons, number of inputs.
Each small change required a 20 minutes to test and for that
few architectures were tested and when one promising architecture was found the exploration was stopped.

\subsection{Unity machine learning agents toolkit}

Given the fact that this work was using \ac{UMLAT} version \textbf{0.9.3}, the scope of the project solutions was limited to the tools present in \ac{UMLAT}.
\ac{UMLAT} was also in beta phase so the presence of bugs or outright broken functionalities was expected. 
Features not present in \ac{UMLAT} at the start of the development of the project were not explored.
Because fixing or adding a feature implied customizing a very volatile code-base it made the modification difficult to maintain.

\section{Training environment architecture}

The architecture used in this project is based on \ac{UMLAT}.
An overview of the architecture can be found in \Cref{fig:training-environment-architecture}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=1.0\textwidth]{./Images/General-pic-a-boo-ai-arch.png}
  \caption{Training environment architecture.}
  \label{fig:training-environment-architecture}
\end{figure}

This project architecture is centered around three entities namely: \textbf{agent}, \textbf{brain}, and \textbf{academy}. 
By examining the diagram in \Cref{fig:training-environment-architecture} the first entity encountered from top to bottom is the \textbf{academy}.
The \textbf{academy} is the entity in charge of orchestrating the environment. This means that it should control the flow of each training session alongside with
resetting each agent to its original state which mean configuring the agents positions, cool-downs and velocities to their initial values.
\\
Being the entity that orchestrates the whole environment the \textbf{academy} was the first to be implemented.
However, Pic-a-boo had several steps before a match could begin for example: using the starting menu, mode selection, selecting player color.
These steps represented an obstacle for the development. 
Trying to implement the academy directly into Pic-a-boo's project structure created unnecessary complexities.
\\
This fact led to the creation of a different Unity project structure called \ac{PAG}.
\ac{PAG} sole objective was to train agents so it didn't need to support the menus and assets present in Pic-a-boo.
However, with the creation of \ac{PAG} came another problem, the trained agents still needed to be used in Pic-a-boo.
To ensure this, \ac{PAG} was created mirroring Pic-a-boo's architecture but eliminating the unnecessary parts and simplifying certain other aspects of the game.
Maps size, number of obstacle, position of the several entities and physics properties were copied from Pic-a-boo.
However, there were also several other simplifications for this project as to test if a basic solution was viable.
The specific simplifications present in \ac{PAG} were:

\begin{itemize}
  \item Only one of the three available stages of the game was present.
  \item Does not include the lamps light.
  \item No vibration on collision.
\end{itemize}

To compensate for this when the player collides it is visible for a few seconds.
With the creation of \ac{PAG} the \textbf{academy} implementation remained simple because it didn't need to interact with Pic-a-boo setup steps.
So the final task for the \textbf{academy} were:

\begin{itemize}
  \item Reposition players to the corner.
  \item Reset the velocities of the player to zero.
  \item Reset flash cool-down to zero.
  \item Set photos counts to zero.
\end{itemize}

The initial state of \ac{PAG} can be seen in \Cref{fig:pag-initial-setup}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.5\textwidth]{./Images/pag-initial-setup.png}
  \caption{Training environment initial setup.}
  \label{fig:pag-initial-setup}
\end{figure}

The next entity to be found in the diagram \Cref{fig:training-environment-architecture} is the \textbf{agent}.
The \textbf{agent} entity is in charge of controlling the different playable avatars while collecting observations and receiving actions from the \textbf{brain}.
\textbf{Agents} collect information about the current game state in the form of observations and send them to the \textbf{brain}.
Following this the \textbf{brain} sends actions to the \textbf{agent} which must be executed in the environment, 
Given the fact that the \textbf{agent} entity was in charge of executing actions, it was attached to the player avatar. 
The player avatar object is the object that the player uses to interact with the game, 
the player avatar was copied from Pic-a-boo preserving the same physics properties.
There were 4 instances of this object running one for each player, 1 human and 3 agents. 
\\
To maintain the separation of concerns the \textbf{agent} script was separated in two parts.
One was the “PlayerController” script, its objective was to receive a series of inputs and execute them in the game environment.
For instance if it received a UP input the “PlayerController” would have pushed the avatar up using Unity physics system.
The second script was the “PlayerAgent” script, its objective was to collect the game state and send it to the \textbf{brain}.
“PlayerAgent” then received an action array that would be mapped to a controller input and be sent to the “PlayerController” for execution.
The values that “PlayerAgent” extracted from the game as observations represents the input of the \ac{ANN} and will be explored in the next section.
The meaning of each value in the action array is given by the \textbf{brain} entity and will be explained alongside the \textbf{brain}.
\\
Finally, the last entity to be implemented was the \textbf{brain} which can be seen in the diagram \Cref{fig:training-environment-architecture} below each agent.
This entity was the one in charge of receiving the current state of the game in the form of observations and pass these values through the \ac{ANN} to obtain a prediction.
The \textbf{brain}
\ac{UMLAT} provides 3 types of brains in the form of an asset:

\begin{itemize}
  \item Learning brain: used to train a new \ac{ANN} model or to make predictions once the model is trained.
  \item Player brain: used to control an agent by receiving humans inputs from a mapped control scheme.
  \item Heuristic brain: used to implement custom algorithm using the \ac{UMLAT} interface.
\end{itemize}

For this project only the learning and player brain were used.
A learning \textbf{brain} represents a bridge to the \ac{ANN} model provided by \ac{UMLAT} in TensorFlow.
To construct a \textbf{brain} entity the only requirements are to define the inputs and outputs of the \ac{ANN} model.
Inputs are defined through the “Vector Observation Size” and will be detailed alongside the \ac{ANN} architecture.
Outputs are defined through the “Vector Action Space” and represents the actions that the \textbf{brain} can take in the form of an N-dimensional float array received by the \textbf{agents}. 
The final parameters for the \textbf{brain} were set in \ac{UMLAT} as:

\begin{itemize}
  \item Vector Observation Size: 19, it represents the number of collected observations, this will be explained in the next section.
  \item Space Type: discrete, which means the values outputted did not use decimals.
  \item Branch size: 4, which means that the float array had a dimension of 4.
  \item Branch 0 size: 3, the branch 0 controls the horizontal movement, 0.0f means do nothing, 1.0f means right, 2.0f means left 
  \item Branch 1 size: 3, the branch 1 controls the vertical movement, 0.0f means do nothing, 1.0f means up, 2.0f means down.
  \item Branch 2 size: 2, the branch 2 controls the Flash, 0.0f means do nothing, 1.0f means Flash.
  \item Branch 3 size: 2, the branch 3 controls the Taunt, 0.0f means do nothing, 1.0f means Taunt.
\end{itemize}

The final configuration can be observed in \Cref{fig:player-brain}.
Following the configuration of the \textbf{brain} it produced outputs in a format like this \Cref{fig:action-sample}.
For the previous example an \textbf{agent} would go left because the first index was 2.0f and up because the second index was 1.0f, When these actions are combined
the avatar moves diagonally going to the top-left side of the map. Finally, it will use taunt because the fourth index was one.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.3\textwidth]{./Images/action_sample.png}
  \caption{This actions means go Northwest and use taunt.}
  \label{fig:action-sample}
\end{figure}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.35\textwidth]{./Images/Player_brain.png}
  \caption{Configuration for a player brain.}
  \label{fig:player-brain}
\end{figure}

A Player \textbf{brain} is the one that a human trying to teach the agent uses, this brain doesn't make predictions the only actions that it emits are the ones
provided by a human teaching the agents, this brains only interacts with TensorFlow to serve as reference for comparisons. 
It Sends observations and actions and receiving nothing from TensorFlow.

\FloatBarrier

To resume the general flow of the architecture is as follows, the \textbf{academy} sets the environment, 
each \textbf{agent} collects observations and sends them to its respective \textbf{brain}.
The \textbf{brain} uses the observations as inputs for the \ac{ANN} implemented in Tensorflow and then receives an output in the form of an action array.
This action array is then sent back to its respective \textbf{agent} which maps the array to an input for the “PlayerController” to execute.
This process is repeated each frame until the game is reset either because one of the players won or the time is over.
Once the game is finished the \textbf{academy} resets the environment and it starts over again.
This loop continues until the maximum iterations are reached.
% https://github.com/Unity-Technologies/ml-agents/issues/3002
% https://github.com/Unity-Technologies/ml-agents/blob/164d1ab98efc620b2e8c18e680e5fc99c19d69f1/ml-agents/mlagents/trainers/bc/models.py
\section{Neural network architecture}

Let's detail the final architecture used for the agents in this project.
Several architectures were tested, ones for instance that had the exact same inputs as the players.
Others had many more values like flash cool-down and remaining targets, however when tested these alternatives did not produce good results.
For more details about the \textbf{alternatives} please check \Cref{section:Alternative architecture}.

As said before, \ac{UMLAT} provides a high-level \ac{API} for creating \acp{ANN} with Tensorflow.
This means that to use and train an \ac{ANN} from the toolkit it wasn't necessary to define nor implement any of the common steps like activation or optimization functions.
Instead, the only requirements were to define the inputs and the topology.
Let's begin by exploring the inputs of the implemented \ac{ANN}.
The inputs are represented by the previously mentioned observations that were collected by the \textbf{agents} which are also related to the vector observation size defined in the \textbf{brain}.
In the \textbf{brain} this value was defined as 19 and it can be divided into 3 categories which are:

\begin{itemize}
  \item Position and velocity of each agent meaning two 2-Dimensional vectors for four agents, which adds to sixteen values, the first set of velocity and position correspond to the player's own.
  \item Position of the last collided object, again a 2-Dimensional vector which adds to two values.
  \item Visibility of the agent, a boolean, which adds to one value.
\end{itemize}

\ac{UMLAT} is capable of training with the values as extracted without any extra treatment, but it is convenient to normalize the values to be between 0 and 1.
The normalization helps speed-up the learning process.

Following this, the next step was to define the network topology.
This is done through the training configuration files provided by \ac{UMLAT} in \cite{umlat:2019}.
Parameters like hidden units and number of layers can be set in these files.
For this implementation the following parameters were used.

\begin{itemize}
  \item Trainer: online\_bc, use the online training as to play at the same time as the training is done.
  \item Brain\_to\_imitate: PlayerWasd, which brain will be performing demonstrations.
  \item Batch\_size: 64, number of experiences in each iteration.
  \item Time\_horizon: 64, how many steps of experience to collect per-agent
  \item Summary\_freq: 1000, how often, in steps, to save training statistics.
  \item Max\_steps: 1.0e4, the maximum number of simulation steps to run during a training session.
  \item Batches\_per\_epoch: 10 in imitation learning, the number of batches of training examples to collect before training the model.
  \item Use\_recurrent: false, train using a recurrent neural network.
  \item Hidden\_units: 128, the number of units in the hidden layers of the neural network.
  \item Learning\_rate: 3.0e-4, the initial learning rate for gradient descent.
  \item Num\_layers: 2, the number of hidden layers in the neural network.
\end{itemize}

Most of the parameters shown above are the default parameters provided by \ac{UMLAT} except for \textbf{Max\_Steps}.
The \textbf{Max\_Steps} value was lowered to 1.0e4 from 1.0e5 because as shown in the graph available in the next chapter \Cref{fig:horizontalActionProgress} after 10000 steps there were no big improvements in the agent quality.
Finally, with these values in place, \ac{UMLAT} creates a Feed-forward \ac{ANN} that can be seen at \Cref{fig:Feed-forward-Neural-Network-Topology}.
\newpage
\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.7\textwidth]{./Images/Neural_Network_Topology.png}
  \caption{Feed-forward Neural Network Topology.}
  \label{fig:Feed-forward-Neural-Network-Topology}
\end{figure}

\section{Neural network training}
The training process of the \ac{ANN} is handled entirely by \ac{UMLAT}.
\ac{UMLAT} provided two type online training and offline training.
To train the \ac{ANN} this project used the online training mode.
In this mode a human must actively play the game against the learning agents.
This mode contrast with the offline training mode in the sense that training offline meant recording the game and feeding the results 
to the \ac{ANN}.
Online training\footnote{During development of this project \ac{UMLAT} decided to deprecate this functionality \cite{umlato:2019} forcing the project to remain in \ac{UMLAT} \textbf{version 0.9.3}} was chosen because it allowed the continuous monitoring of the agents.
If left to train alone following the recordings the training iterations took more time because 
at the beginning the agents aren't able to finish a match.

After setting the training type, how \ac{UMLAT} handles the training is not documented. 
However, the code is open-source so it was reverse engineered.
A general overview of the training architecture can be seen at \Cref{fig:Neural-Network-training}.

The training is done using a python environment with the TensorFlow library.
First the parameters defined in the configuration files are received and the topology of the \ac{ANN} is created.
Second the observations stream composed of the previously mentioned positions and velocities reaches the \ac{ANN} 
where the stream is multiplied by the weights-matrix and summed by the bias.

\begin{verbatim}
  outputs = linearActivation(inputs * weights-matrix + bias)
\end{verbatim}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=1.0\textwidth]{./Images/neural-network-training-architecture.png}
  \caption{Neural Network training architecture.}
  \label{fig:Neural-Network-training}
\end{figure}

The values then pass through a linear activation function which controls when the neurons are active.
This process is repeated as many times as there are layers. 
For this project the process was repeated two times because there were only two layers.
The final step to obtain the predictions is to pass through a soft max function.
The soft max function is going to normalize the result and perform a logistic regression.
Logistic regression are used to predict categorical results like the ones in Pic-a-boo, for instance flashing and not flashing \cite{Bishop:2006}.

With the prediction finally obtained, the value is then compared to the human \textbf{brain} action. 
From this comparison the loss value is obtained and then the value is passed through an Adam optimizer.

The Adam optimizer is a variant of a stochastic gradient descent function, these functions are used to calculate 
a local minimum by approximating the gradient descent instead of calculating it with the whole data.
This estimation is done by estimating it with part of the data.
These functions are commonly used as an optimization algorithm 
to reduces loss and updates the network weights based on the training data \cite{Diederik:2015}.

The process described above is repeated continuously in a training session, 
so each time the weights are updated, the prediction of the \ac{ANN} changes to be more similar to the real action of the human because the 
Adam optimizer reduces the difference between both values.

\section{Exporting agents to Pic-a-boo}

To use the agents inside Pic-a-boo there were two things required.
One was the model from \ac{PAG} produced by \ac{UMLAT} which is a binary that represents the trained brains.
The second was to add \ac{UMLAT} to Pic-a-boo main project, the process was similar. It was required to include the
\textbf{Brain}, \textbf{Agent} an \textbf{Academy} entities. 
This was an easy process because \ac{PAG} architecture is a copy of Pic-a-boo, so it was a matter of copying the code.
More details of the implementation process can be found in \Cref{section:Pic-a-boo implementation}.
A video of the final result can be seen in \href{https://www.youtube.com/watch?v=cvScIMoUR-E}{here}

\section{Agent quality tests}

To test the agents, it was necessary to enter an input to the \ac{ANN} and get a prediction.
The inputs of the \ac{ANN} are the observations extracted from the game-state as mentioned before.
So it was required to save these observations to later input them into the \ac{ANN}.
A small modification was done in the \textbf{agent} entity for it to record a player match into a JSON file.
The JSON file contained the various observations of the game space through each frame alongside with what actions the human performed based on that observation.
The values that the JSON file contained were:

\begin{itemize}
  \item player: contains an object describing the player position and velocity.
  \item targets: contains an array objects describing the targets position and velocity.
  \item isVisible: a flag describing if the player avatar was visible.
  \item lastObstacleX: describe the horizontal position of the last collision with an object.
  \item lastObstacleY: describe the horizontal position of the last collision with an object
  \item horizontalMovement: The output horizontal action made by the player based on the above observation. Zero neutral, one right, two left.
  \item verticalMovement: The output vertical action made by the player based on the above observation. Zero neutral, one up, two down.
  \item flash: The output flash action made by the player based on the above observation. Zero neutral, one flash.
  \item taunt: The output taunt action made by the player based on the above observation. Zero neutral, one taunt.
\end{itemize}

These values correspond with the inputs and outputs of the \ac{ANN}.
An example file can be seen in \Cref{fig:save_format}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.35\textwidth]{./Images/save_format.png}
  \caption{Saved JSON.}
  \label{fig:save_format}
\end{figure}

Following the recording of various matches, this data was inputted into the \ac{ANN} to obtain a prediction.
This prediction was then compared to the real action that the human did.
To compare the predicted actions and real actions, 4 confusion matrices with their respective parameters \footnote{sensitivity, specificity, accuracy, precision and f1Score} were calculated.
One for each index, horizontal, vertical, flash and taunt.
For flash and taunt a binary matrix was used, which can be seen in \Cref{fig:flash_matrix} and in \Cref{fig:taunt_matrix}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.4\textwidth]{./Images/ConfusionMatrixFlash.png}
  \caption{Flash matrix example.}
  \label{fig:flash_matrix}
\end{figure}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.4\textwidth]{./Images/ConfusionMatrixTaunt.png}
  \caption{Taunt matrix example.}
  \label{fig:taunt_matrix}
\end{figure}

In contrast, for the horizontal and vertical movement a multi-class matrix was used.
An example of these matrices can be seen in \Cref{fig:horizontal_matrix} and in \Cref{fig:vertical_matrix}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.5\textwidth]{./Images/ConfusionMatrixHorizontal.png}
  \caption{Horizontal matrix example.}
  \label{fig:horizontal_matrix}
\end{figure}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.5\textwidth]{./Images/ConfusionMatrixVertical.png}
  \caption{Vertical matrix example.}
  \label{fig:vertical_matrix}
\end{figure}

\newpage

Alongside these matrices two more metrics were calculated the previously mentioned hamming loss and the perfect match.
The training and test process was repeated with 6 different people plus the researcher.
The data produced from these tests will be explored in the next chapter.

\section{Human assessment tests}

Previously, it was explained that a human-test was required to assess the quality of the agent.
For this test, a query was created for the cloned subjects.
The query consisted of 4 match videos. In the matches there were four clones. 
One clone was the subject clone, 
another one was a control agent that trained just 10 iterations so it can be considered a random clone,
the remaining players were other clones selected randomly.
The first video was a training video, all agents inside the video were identified by the color of their avatar
in the title of the video.
For the next three videos the agents starting position and colors were switched.
The query presented then 3 questions.

\begin{itemize}
  \item What color is your clone?
  \begin{itemize}
    \item Red
    \item Blue
    \item Green
    \item Yellow
  \end{itemize}
  \item How confident is your choice?
  \begin{itemize}
    \item Strongly Agree
    \item Agree
    \item Partially Agree
    \item Disagree
  \end{itemize}
  \item Why did you choose that clone?
\end{itemize}

The first two questions were multi-selection and the last one was an open-ended question.
This query was made to understand if the clone's owner was capable of differentiating between the others thus implying there are subjective differences between the clones.
Being four clones the possibility of choosing randomly and matching the correct clones is feasible. 
For that reason the confidence and reasoning behind the selection was asked.
The results of this query are presented in the next chapter.

\section{Closing remarks}
By using \ac{UMLAT} as a base, this project managed to rapidly produce a working \ac{AI} for Pic-a-boo,
we could also see that \ac{UMLAT}'s three entity architecture was versatile enough to be applicable for Pic-a-boo.
The \textbf{brain}, \textbf{agent}, and \textbf{academy} architecture works very well for \ac{AI} development because it defines clear objectives for each entity 
and can serve as a guide in environments outside of unity and \ac{UMLAT}.
However, for all the good that \ac{UMLAT} brought it also limited the available options of \ac{ANN} types
to what was present in the beta.

% Aliquam aliquet, est a ullamcorper condimentum, tellus nulla fringilla elit, a iaculis nulla turpis sed wisi. Fusce volutpat. Etiam sodales ante id nunc. Proin ornare dignissim lacus. Nunc porttitor nunc a sem. Sed sollicitudin velit eu magna. Aliquam erat volutpat. Vivamus ornare est non wisi. Proin vel quam. Vivamus egestas. Nunc tempor diam vehicula mauris. Nullam sapien eros, facilisis vel, eleifend non, auctor dapibus, pede. 
% % #############################################################################
% \section{Development Process}
% Suspendisse vestibulum dignissim quam. Integer vel augue. Phasellus nulla purus, interdum ac, venenatis non, varius rutrum, leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis a eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce magna mi, porttitor quis, convallis eget, sodales ac, urna. Phasellus luctus venenatis magna. Vivamus eget lacus. Nunc tincidunt convallis tortor. Duis eros mi, dictum vel, fringilla sit amet, fermentum id, sem. Phasellus nunc enim, faucibus ut, laoreet in, consequat id, metus. Vivamus dignissim. Cras lobortis tempor velit. Phasellus nec diam ac nisl lacinia tristique. Nullam nec metus id mi dictum dignissim. Nullam quis wisi non sem lobortis condimentum. Phasellus pulvinar, nulla non aliquam eleifend, tortor wisi scelerisque felis, in sollicitudin arcu ante lacinia leo.:
% 
% \begin{itemize}
% \item{Technology Research and Related Works}
% \item{Requirements Gathering and Study}
% \item{Design of the Architecture}
% \item{Implementation Process}
% \item{Testing and Functional Validation}
% \end{itemize}
% 
% Pellentesque nibh felis, eleifend id, commodo in, interdum vitae, leo. Praesent eu elit. Ut eu ligula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Maecenas elementum augue nec nisl. Proin auctor lorem at nibh. Curabitur nulla purus, feugiat id, elementum in, lobortis quis, pede. Vivamus sodales adipiscing sapien. Vestibulum posuere nulla eget wisi. Integer volutpat ligula eget enim. Suspendisse vitae arcu. Quisque pellentesque. Nullam consequat, sem vitae rhoncus tristique, mauris nulla fermentum est, bibendum ullamcorper sapien magna et quam. Sed dapibus vehicula odio. Proin bibendum gravida nisl. Fusce lorem. Phasellus sagittis, nulla in hendrerit laoreet, libero lacus feugiat urna, eget hendrerit pede magna vitae lorem. Praesent mauris.
% % #############################################################################
% \section{Development Environment}
% Cras sed ante. Phasellus in massa. Curabitur dolor eros, gravida et, hendrerit ac, cursus non, massa. Aliquam lorem. In hac habitasse platea dictumst. Cras eu mauris \Cref{time_control_algorithm}\todo[color=cyan!40, author=RC, fancyline]{Notice the reference to the Algorithm construct}{}. Quisque lacus. Donec ipsum. Nullam vitae sem at nunc pharetra ultricies. Vivamus elit eros, ullamcorper a, adipiscing sit amet, porttitor ut, nibh. 
% 
% \begin{algorithm}[ht]
% \DontPrintSemicolon
% \Begin{
% $nextBitrate \longleftarrow nextDownloadLevel$\;
% $nextBitrate \longleftarrow GetNextBitrate()$\;
% $cpuLoad \longleftarrow GetCpuLoad()$\;
% $bitrateDelta \longleftarrow getBitrateDelta(currentBitrate, nextBitrate)$\;
% \BlankLine
% \If{$bitrateDelta > maxThreshold$}{
%      $SetBitrate(nextBitrate)$\;
%    }
% \BlankLine
%   \If{$minThreshold < bitrateDelta < maxThreshold$ {\bf and} $numAttemps < 2$}{ 
%        $numAttemps \longleftarrow numAttemps + 1$\;
%        }{
%        \uElseIf{$minThreshold < bitrateDelta < maxThreshold$ {\bf and} $numAttemps = 2$}{
%        $numAttemps \longleftarrow 0$\;
%        }
%        \Else{$SetBitrate(nextBitrate)$}
%       }
%   \If{$0 < bitrateDelta < minThreshold$ {\bf and} $numAttemps < 3$}{
%        $numAttemps \longleftarrow numAttemps + 1$\;
%        }{
%        \uElseIf{$0 < bitrateDelta < minThreshold$ {\bf and} $numAttemps = 3$}{
%        $SetBitrate(nextBitrate)$\;
%        }
%        }
% }
% \caption{Time Control Strategy}
% \label{time_control_algorithm}
% \end{algorithm}
% 
% 
% Maecenas adipiscing mollis massa. Nunc ut dui eget nulla venenatis aliquet. Sed luctus posuere justo. Cras vehicula varius turpis. Vivamus eros metus, tristique sit amet, molestie dignissim, malesuada et, urna..
% % #############################################################################
% \section{Client Application}
% Cras sed ante. Phasellus in massa. Curabitur dolor eros, gravida et, hendrerit ac, cursus non, massa. Aliquam lorem. In hac habitasse platea dictumst. Cras eu mauris. Quisque lacus. Donec ipsum. Nullam vitae sem at nunc pharetra ultricies. 
% 
% Vivamus elit eros, ullamcorper a, adipiscing sit amet, porttitor ut, nibh. Maecenas adipiscing mollis massa. Nunc ut dui eget nulla venenatis aliquet. Sed luctus posuere justo. Cras vehicula varius turpis. Vivamus eros metus, tristique sit amet, molestie dignissim, malesuada et, urna.
% 
% Quisque lacus. Donec ipsum. Nullam vitae sem at nunc pharetra ultricies. Cras vehicula varius turpis.
% 
% 
% 
% \begin{minipage}[c]{1.0\textwidth}
% %\begin{center}
% \centering
% \begin{lstlisting}[language = C++, numbers = none, escapechar = !,
%     basicstyle = \ttfamily\bfseries, linewidth = .6\linewidth, frame=tb, caption={A listing with a Tikz picture overlayed}, captionpos=b, label=tikzlist] 
%  int!
%    \tikz[remember picture] \node [] (a) {};
%  !puissance!
%    \tikz[remember picture] \node [] (b) {};
%  !(int x,!
%    \tikz[remember picture] \node [] (c){};
%  !int n) { 
% 
%      int i, p = 1; !\tikz[remember picture] \node [] (d){};!           
% 
%      for (i = 1; i <= n; i++) 
%        p = p * x; !\tikz[remember picture] \node [inner xsep = 40pt] (e){};! 
% 
%      return p; !
%        \tikz[remember picture] \node [] (f){};!  
%  }
% \end{lstlisting}
% 
% \begin{tikzpicture}[remember picture, overlay,
%     every edge/.append style = { ->, thick, >=stealth,
%                                   darkgray, dashed, line width = 1pt },
%     every node/.append style = { align = center, minimum height = 10pt,
%                                  font = \bfseries, fill= green!20},
%                   text width = 2.5cm ]
%   \node [above left = .75cm and -.75 cm of a,text width = 2.2cm]
%                              (A) {return value type};
%   \node [right = 0.25cm of A, text width = 1.9cm]
%                              (B) {function name};
%   \node [right = 0.5cm of B] (C) {list of formal parameters};
%   \node [right = 4.cm of d]  (D) {local variables declaration};
%   \node [right = 2.cm of e]  (E) {instructions};
%   \node [right = 5.cm of f]  (F) {instruction \texttt{\bfseries return}};  
%   \draw (A.south) + (0, 0) coordinate(x1) edge (x1|-a.north);
%   \draw (B.south) + (0, 0) coordinate(x2) edge (x2|-b.north);
%   \draw (C.south) + (0, 0) coordinate(x3) edge (x3|-c.north);
%   \draw (D.west) edge (d.east) ;
%   \draw (E.west) edge (e.east) ;  
%   \draw (F.west) edge (f.east) ;
% \end{tikzpicture}
% %\end{center}
% \end{minipage}
% 
% \textcolor{violet}{And here another method (\Cref{tikzlist}) for mixing (overlay) a picture with a listing of code.}
% % #############################################################################
% \subsection{User Interface}
% Donec semper turpis sed diam. Sed consequat ligula nec tortor. Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit. Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl. Vivamus luctus elit sit amet mi. Phasellus pellentesque, erat eget elementum volutpat, dolor nisl porta neque, vitae sodales ipsum nibh in ligula. Maecenas mattis pulvinar diam. Curabitur sed leo..
% 
% Cras eu mauris. Quisque lacus. Donec ipsum. Nullam vitae sem at nunc pharetra ultricies. Vivamus elit eros, ullamcorper a, adipiscing sit amet, porttitor ut, nibh. Maecenas adipiscing mollis massa. Nunc ut dui eget nulla venenatis aliquet. Sed luctus posuere justo. Cras vehicula varius turpis. 
% % #############################################################################
% \subsection{Vivamus luctus elit sit amet mi}
% Nulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo, sed imperdiet lectus est ornare odio. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin consectetuer velit in dui. Phasellus wisi purus, interdum vitae, rutrum accumsan, viverra in, velit. Sed enim risus, congue non, tristique in, commodo eu, metus. Aenean tortor mi, imperdiet id, gravida eu, posuere eu, felis. 
% 
% Mauris sollicitudin, turpis in hendrerit sodales, lectus ipsum pellentesque ligula, sit amet scelerisque urna nibh ut arcu. Aliquam in lacus. 
% 
% \Cref{fig:ui_playout,fig:ui_loading}\todo[color=cyan!40, author=RC, fancyline]{A figure with Subfigures}{} proin at eros non eros adipiscing mollis.
% 
% \begin{figure}[htbp]
% 	\centering
% 	\subfigure[Media Loading Window]{\label{fig:ui_loading} 		\includegraphics[width=0.3\textwidth]{./Images/ui_loading}} \qquad
% 	\subfigure[Play-out Session \ac{UI}]{\label{fig:ui_playout}
% 		\includegraphics[width=0.3\textwidth]{./Images/ui_playout}}
% 	\caption{Complete User Interface}
% 	\label{fig:user_interface}
% \end{figure}
% 
% Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla placerat aliquam wisi. Mauris viverra odio. Quisque fermentum pulvinar odio. Proin posuere est vitae ligula. Etiam euismod. Cras a eros.