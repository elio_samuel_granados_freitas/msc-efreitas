% #############################################################################
% This is Chapter 3
% !TEX root = ../main.tex
% #############################################################################
% Change the Name of the Chapter i the following line
\fancychapter{Game Description}
\cleardoublepage
% The following line allows to ref this chapter
\label{chap:gdescription}
\section{Chapter description}
In this chapter Pic-a-boo's rules and components will be detailed to extract the parameters that the agents will follow while playing.
The first section gives a general overview of how the game is played. The next section details the different actions that can be performed.
Finally, the third section details the map.

\section{Game concept}
Pic-a-boo is a game whose main objective is taking pictures of enemy players.
The game is played in a top-down view map which can be seen in \Cref{fig:pic-a-boo-counter} with a two-dimensional movement.
The screen is shared between all players so each player has the same information as their adversaries.
Players may move in horizontal, vertical and diagonal directions
\footnote{Gameplay videos can be found in \href{https://www.youtube.com/watch?v=4IB3gs9cq5g&feature=emb_title}{here} or \href{https://www.youtube.com/watch?v=cvScIMoUR-E}{here}}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\textwidth]{./Images/Game_Counter.png}
    \caption{Blue player current photograph.}
    \label{fig:pic-a-boo-counter}
\end{figure}

Taking pictures of the player is done by using the flash action near an enemy.
Being photographed does not mean you are out of the game.
All enemies must be captured to win the game, repeated photos don't count.
Taking a photo inside the game means first navigating near the position of your target.
Next, with the avatar near the target the player must press the flash button action.
The flashlight direction cannot be controlled independently it will point at the last direction that the player moved.
The flashlight is represented by a cone of light being emitted from the player avatar which can be seen in \Cref{fig:pic-a-boo-cone}.
A target must be inside the cone of light when it is initially pressed. 
In case the target is illuminated by the cone but it wasn't present in the initial flash it won't count.
Capturing a target with the initial flash will cause the target to enter a flashed state which can be exemplified by the red player in \Cref{fig:pic-a-boo-counter}.
In this flashed state the enemy can only move but may not perform any other action.

Given the fact that the map is dark the players can only see themselves near the light emitted by the lamps or after using the flash action.
In addition, the player may also use the taunt action to see their player avatar without a light source and to lure the other players.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\textwidth]{./Images/Cone-of-light.png}
    \caption{Yellow player using its flash.}
    \label{fig:pic-a-boo-cone}
\end{figure}

\section{Game Actions}

The game may be played with either a keyboard or a controller.
In game, players can perform 6 actions which are:

\begin{itemize}
    \item LEFT(A): Starts moving the player's avatar horizontally to the left.
    \item RIGHT(D): Starts moving the player's avatar horizontally to the right.
    \item UP(W): Starts moving the player's avatar vertically to the top.
    \item DOWN(S): Starts moving the player's avatar vertically to the bottom.
    \item FLASH(G): Turns on the player flashlight.
    \item TAUNT(T): Highlights the player showing its current position.
\end{itemize}

The controller button scheme may vary depending on the controller.
The vertical and horizontal movement actions can be combined to move in a diagonal.
For instance if the player is moving UP and combines it with LEFT the avatar starts moving in a 45 degree diagonal to the north-west.
The avatar's velocity is constant so it can go from 0 to maximum speed with just one action.
Colliding with a tomb stops the player's avatar, colliding with an enemy causes the avatar to starts pushing the enemy in the same direction the player's avatar is going.
The enemy may stop the push by walking in a direction opposite to where he is being pushed.

Flashing allows capturing enemies in a photo, however flashing has a cool-down period of 3 seconds.
Following the use of the flash action the bar near the player's portrait will begin to refill itself representing the cool-down period.
The player should be able to use flash again once the bar is refilled, these two states can be seen in \Cref{fig:pic-a-boo-cooldown-timers}.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{./Images/cooldown-timers.png}
    \caption{Refilling flash bar.}
    \label{fig:pic-a-boo-cooldown-timers}
\end{figure}

Taunting is an action that serves two functions. 
The first one is to help the players find themselves the map without resorting to their flashlight.
This is because the flashlight has a cool-down. Should the player use the flash to find themselves it will mean losing the chance to take a picture, remaining in a vulnerable state during the cool-down.
The second use is to lure enemy players. The idea is to use the taunt action to highlight themselves tempting enemies to go after them.
Taunting has no cool-down period and may be used constantly. The appearance of the taunt action can be seen at \Cref{fig:pic-a-boo-flashing-player}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.3\textwidth]{./Images/flashing-player.png}
    \caption{A player using the taunt action.}
    \label{fig:pic-a-boo-flashing-player}
\end{figure}

\FloatBarrier

\section{Map}

Pic-a-boo currently offers 3 maps that are chosen at random before starting a match.
The levels are played in a top-down view which can be appreciated in \Cref{fig:pic-a-boo-map}.
A top-down view is achieved by positioning a camera above the map and looking down in a 90 to 45 degrees.
Given the type of view, the player navigation is limited to a two-dimensional movement scheme, to be specific vertical, horizontal and diagonal movement.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\textwidth]{./Images/map_elements.png}
    \caption{Various map elements.}
    \label{fig:pic-a-boo-map}
\end{figure}

All the players share the same view, so every player can see his own avatar and the avatar of their enemies.
The only thing stopping the players from seeing everything on the map is the fog of war presented by the lack of light in the room.
Light sources in the map come from 2 sources the first one comes from the lamps that are all around the map.
The light emitted by the various lamps is turned on randomly and remains lit a limited amount of time illuminating a small radius around it which can be seen in \Cref{fig:pic-a-boo-map}.
The second light source comes from the player flash which as said before is a cone coming from the player avatar.
Regarding collisions the player can collide with the small tombs present around the map (see \Cref{fig:pic-a-boo-map}).
A wall surrounds the playing area preventing players from leaving the game area.
At this stage of development there are no moving obstacles in the game.
Players can collide between themselves and be pushed around by other player.
When a player collide with an obstacle or between each other the UI gets highlighted from the side it collided.
Obstacles don't block light so currently players can be photographed over an obstacle.

\section{Closing remarks}

Now to recapitulate the knowledge we need to continue, to interact with the game the agent that is going to be created,
needs to be able to perform the 6 actions available in Pic-a-boo namely up, left, right, down, flash and taunt.
To be able to do this, these actions are going to represent the output of the \ac{ANN},
The \ac{ANN} is going to output an array that should represent these actions which then will be mapped into the game to be executed by the avatar.
As for the inputs, there are several things to take into account. 
As previously said this project is going to use a \textbf{symbolic} input so that means data must be extracted directly from the game state.
The most basic values needed to make any kind of prediction by the \ac{ANN} are the position and velocity of each avatar, 
other values that might be useful are the cool-down of the flash, the visibility of each agent the current state of each lamp and the photos
required to win theses will be detailed in the next chapter.

 % Donec gravida posuere arcu. Nulla facilisi. Phasellus imperdiet. Vestibulum at metus. Integer euismod. Nullam placerat rhoncus sapien. Ut euismod. Praesent libero. Morbi pellentesque libero sit amet ante. Maecenas tellus. Maecenas erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
 % % #############################################################################
 % \section{Architecture Design Requirements} 
 % \textcolor{violet}{Example of a Flowchart for a system, in \Cref{fig:flowchart}, created with \url{https://www.draw.io} and then exported as ``PDF'' crop format (a true vector image that can be scaled to no end, with no pixels or distortion).}
 % 
 % \begin{figure}[h]
 % \centering
 % \includegraphics[width=1.0\textwidth]{./Images/Flowchart_from_draw-io.pdf}
 % \caption{System Processes}
 % \label{fig:flowchart}
 % \end{figure}
 % 
 % Quisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. 
 % 
 % \textcolor{violet}{And here another diagram of a network (\Cref{fig:network}) created with \url{https://www.draw.io} and then exported as ``PDF'' crop format.}
 % 
 % \begin{figure}[h]
 % \centering
 % \includegraphics[width=1.0\textwidth]{./Images/Network_from_draw-io.pdf}
 % \caption{Network Diagram}
 % \label{fig:network}
 % \end{figure}
 % 
 % Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc:
 % 
 % \begin{description}
 % 	\item[\textbf{Web-streaming:}]
 % 	The client application should support streaming media using \ac{HTTP} protocols.
 % 	\item[\textbf{Multi-source streaming:}]
 % 	The client application should support multi-source streaming media, i.e., ``simultaneous'' streaming of media content components from a network, supported\slash complemented by \ac{CDN}\slash \ac{CC} services. 
 % 	\item[\textbf{Support content Metadata Description:}]
 % 	The client application should support content metadata description in a format similar or compliant with MPEG \ac{DASH} \cite{ISO/IEC:2012fk}. 
 % 	\item[\textbf{Scalable and Adaptive Media Contents:}]
 % 	The system should support on-demand streaming of scalable and adaptive contents based on \ac{SVC}.
 % 	\item[\textbf{Heterogenous End-User Devices:}]
 % 	The client application should be compatible with current and future generations of end-user devices form factors, irrespective of their performance, screen size and resolution.
 % 	\item[\textbf{Access Network independency:}] 
 % 	The solution should provide the expected service over different types of access networks supported by the end-user devices, such as Wireless \acp{LAN} (IEEE 802.11) or cellular data networks such as \ac{GPRS}, \ac{UMTS}, \ac{LTE}, etc.
 % \end{description}
 % 
 % Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis.
 % % #############################################################################
 % \section{Architecture Design Requirements}
 % Ut nulla. Vivamus bibendum, nulla ut congue fringilla, lorem ipsum ultricies risus, ut rutrum velit tortor vel purus. In hac habitasse platea dictumst. Duis fermentum, metus sed congue gravida, arcu dui ornare urna, ut imperdiet enim odio dignissim ipsum. Nulla facilisi. Cras magna ante, bibendum sit amet, porta vitae, laoreet ut, justo. Nam tortor sapien, pulvinar nec, malesuada in, ultrices in, tortor. Cras ultricies placerat eros. Quisque odio eros, feugiat non, iaculis nec, lobortis sed, arcu. Pellentesque sit amet sem et purus pretium consectetuer \Cref{mpd}\todo[color=cyan!40, author=RC, fancyline]{A listing for XML code, with syntax highlighting}{}.
 % 
 % \begin{minipage}[c]{0.95\textwidth}
 % \begin{center}
 % \begin{spacing}{0.5}
 % \begin{lstlisting}[frame=lines,style=XML,caption={Example of a MPD file.},label=mpd]
 % <?xml version="1.0" encoding="UTF-8"?>
 % <StreamInfo version="2.0">
 %     <Clip duration="PT01M0.00S">
 %         <BaseURL>videos/</BaseURL>
 %         <Description>svc_1</Description>
 %         <Representation mimeType="video/SVC" codecs="svc" frameRate="30.00" bandwidth="401.90"
 %             width="176" height="144" id="L0">
 %             <BaseURL>svc_1/</BaseURL>
 %             <SegmentInfo from="0" to="11" duration="PT5.00S">
 %                 <BaseURL>svc_1-L0-</BaseURL>
 %             </SegmentInfo>
 %         </Representation>
 %         <Representation mimeType="video/SVC" codecs="svc" frameRate="30.00" bandwidth="1322.60"
 %             width="352" height="288" id="L1">
 %             <BaseURL>svc_1/</BaseURL>
 %             <SegmentInfo from="0" to="11" duration="PT5.00S">
 %                 <BaseURL>svc_1-L1-</BaseURL>
 %             </SegmentInfo>
 %         </Representation>
 %     </Clip>
 % </StreamInfo>
 % \end{lstlisting}
 % \end{spacing}
 % \end{center}
 % \end{minipage}
 % 
 % Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam.