% #############################################################################
% This is Chapter 2
% !TEX root = ../main.tex
% #############################################################################
% Change the Name of the Chapter i the following line
\fancychapter{Related Work}
\cleardoublepage
% The following line allows to ref this chapter
\label{chap:relatedwork}

% #############################################################################

\section{Chapter description}
In this chapter various related topics will be explored to gather the required knowledge needed to proceed with the project.
The first topic is going to be Player modelling to understand the different approaches to imitate a player.
Following player modelling, the next topic will be neural networks. In this section previous works will be explored to find a good configuration
for the \ac{ANN} that will be used. 
Pic-a-boo is developed in Unity, Unity provides several libraries for different purpose one of them being machine learning.
So the fourth section will be dedicated to exploring its capabilities.
The final section will be about how to measure \ac{AI} with the aim of exploring the methodologies that had been 
used previously to determine the quality of the resulting \ac{AI}.

\section{Player Modelling}

Player modelling is the study of computational models of players in games.
It includes the detection, modelling, prediction, and expression of
human player characteristics. These characteristics manifest themselves through
cognitive, affective and behavioral patterns. The main goal of player modelling
research is to understand how players interact with a game. Following this, a model is produced
based upon that interaction \cite{Yannakakis:2013}.

While playing video games humans produce dynamic and complex behaviors.
There are 2 approaches to model these complex behaviors when creating player models.
The first approach is a theory based approach which relies on making a rough approximation of the human's behavior.
The agents produced from this method will behave in the predetermined behavior or strategy, for example fleeing attackers.
According to \cite{Holmgard:2014} we can call this model-based approach a \textbf{persona} or personality.
The second approach is a data-driven approach which rely on gathering data from players to analyze the patterns inside the data.
The agents produced from this method will then behave in a way that agrees with the patterns extracted from the play traces.
We can call this data-driven approach a \textbf{clone}.

Both cases have problems, for instance, clones are prone to over-fitting, where the agent only learns to cope with situations appearing in the play
traces. In the case of new situations, the agent might have irregular behavior.
Personalities to a large extent solve the irregular behavior problem. The
solution provided is through learning a robust general strategy. This solution is
also called a decision-making style. But a decision-making style can be general
and predictable.

This project requires choosing between clones or personas. 
Trying to formulate a general strategy to imitate a player is difficult and will lead to a predictable agent.
Guesswork is one of the main components of Pic-a-boo's matches so being predictable would eliminate a big part of the challenge. 
The cloning method might bring irregular behavior but humans are not a regular creature so it isn't a big issue.
So between the two the cloning methodology is the chosen.

\section{Artificial Neural Networks}

Artificial Neural networks (\ac{ANN}) are a family of algorithms used to simulate human capabilities among other uses, 
for instance image recognition, face identification and self driving cars \cite{Simonyan:2016}. 
These tasks are easy for humans to perform but not for a computer. 
This is because it’s not easy to describe these task in a couple
of reproducible steps. 
It is not easy because the numbers of steps might be too big or have too many caveats, thus making it impractical to code.
\ac{ANN} overcome this problem by imitating a human brain.

There are various examples of using an \ac{ANN} for creating human behavior, for instance
the company \footnote{King Digital Entertainment} behind candy crush saga\footnote{Candy crush saga\href{https://www.youtube.com/watch?v=d5Rf0An-jEg}{demo}}. 
The game is shown in \Cref{fig:candy-crush-saga-gameplay}.
The company
sought to improve their game testing by using a
\ac{CNN}. \ac{CNN} is a specific type of \ac{ANN}. \ac{CNN} original purpose was for image-like
data feature extraction.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/candy_crush.jpg}
    \caption{Candy Crush Saga puzzle.}
    \label{fig:candy-crush-saga-gameplay}
\end{figure}

The need came from the speed at which they wanted to release new content.
The speed of release was not aligned with the velocity of the testing efforts. This because humans were required to test the product. Human testing comes with
the disadvantage of introducing latency and costs \cite{Gudmundsson:2017}. They saw an advantage in
creating a human behaving algorithm to automate testing. The idea was that an
agent could run tests at any time after the new levels were completed. 
These automated tests would help find problems in the new levels increasing the speed of development and reducing the costs of
human testers.

According to \cite{Gudmundsson:2017} there have been many attempts at creating a human agent,
for example simulating a player playing a level using an agent with a simple heuristic. But
these approaches lack one important aspect. 
The approaches were trying to imitate humans without human input. 
For this fact the researchers sought to include human data because they thought it could be beneficial.

The general approach they used was an agent that would play the game
with the help of a \ac{CNN} to guide decisions. The agent would then generate the
metric of interest needed to test the quality of the levels. Then the researchers would
compare the values from the agent with the values from humans. The closer the
strategy of the agent to that of human players the better.

Researchers compared the approach against another approach using
\ac{MCTS}. The decision to benchmarks with an \ac{MCTS} agent came out of pragmatism. 
It was practical to use because the company already had a working \ac{MCTS} agent. So it was useful
to compare the two to check if the new approach was better than the previous
one.
The conclusion was that the new approach outperformed the \ac{MCTS} agent.
The \ac{CNN} agent had more predictive power, execution efficiency and
also outperformed humans. Human play testers took around 7 days to complete
a level. Instead, the new \ac{CNN} agent could perform the task in less than a minute
which is a huge optimization of time.

Another example of using an \ac{ANN} for creating a human agent came from 
the researchers from the University of Essex. Their goal was to create a general game playing \ac{AI}. 
They used a \ac{CNN} paired with a Q-learning algorithm. 
This solution is called Deep-Q-Network which is a combination between a \ac{CNN} and Q-learning. The \ac{CNN}
would analyze the image translate it into inputs and make actions. While the
Q-learning functioned as a training supervisor for the \ac{CNN}. Q-learning would
analyze the state-action relationship to select what data to feed into the \ac{CNN} to train, based on previous states.

The researchers decided to use screen capture to receive the inputs of the
games, which is a \textbf{sub-symbolic} input source. Meaning it is made by constituent
entities that are not direct representations of the game state, for example pixels. In contrast
to using \textbf{symbolic} inputs, which are a representation system in which the atomic
constituents are a direct representation of the game state. For example the
system recording the player position \cite{Kunanusont:2017}.

The decision came because humans receive most of their information through
visual sensors. The reception of the inputs from a visual source is beneficial for
a general game playing \ac{AI}. Because this allows decoupling the algorithm from
the game software, in turn increasing its adaptability to other problems.

The method used contains two steps. The first step was to process the image,
they shrank each defined block down into one pixel. Then normalized the RGB
value and expanded into the smallest size allowed. The modification of the image
normalized the inputs. The normalization need came because the \ac{CNN} structure
is the same for all games. Also, the algorithms could get overloaded with the whole
information contained in the image.

For the second step the Q-learning algorithm consumes the image as a current
state analyzing the reward. The Q-learning will choose the next action to train the \ac{CNN} based on the Q values.
The \ac{CNN} then receives the input image and generates an action.

Results were positive, the agent was able to learn how to solve both static 
\footnote{Game in which a single decision is made by each player,
and each player has no knowledge of the decision made by the other
players}
and stochastic
\footnote{Game where each player selects an action and receives a payoff, the games moves to a new random state based on the previous state and actions}
games. The accumulative winning percentage in static games
increased the more the agent played. The same applied to the cumulative average
score in stochastic games. Suggesting the agent was applying knowledge acquired
during the previous plays.

In general after checking these works it seems feasible to use an algorithm from the \ac{ANN} family.
Candy crush is a game played in real time proving that \ac{ANN} based solutions is capable of fast response times.
Regarding the work done on the university of Essex, they demonstrate the
ability of a neural network to play not only one game but several. Even more in
a learning style close to a human because it is based on vision. While the goal was different,
because they wanted to achieve a general perfect-play agent and this work
seeks to create a human-like agent.

One of the main decision lies in how the neural network will receive data. 
As previously mentioned there are 2 approaches to the inputs \textbf{symbolic} or \textbf{sub-symbolic}.

The main problem of using a neural network is the training phase because
this process requires an adequate quantity of data. The quantity of
data is difficult to assess but is related to how many scenarios the \ac{ANN}
requires learning. The problem is there is no available data already created like
for the candy crush saga example. Data collection requires recording several
play-through of the game. These recording are time-consuming.

A \textbf{sub-symbolic} approach like screen capturing brings the need of analyzing, compressing and simplifying the video.
Otherwise, the amount of data to input into the \ac{ANN} would be too much hampering the training times. Also,
Pic-a-boo is a game played with fog of war. Which does not let you see your character or the enemy character. 
This means it is an imperfect information game. So a big chunk of information represented in
the video will be useless to train requiring more refining efforts.

This fact shift the balance to favor using \textbf{symbolic} inputs. 
A symbolic data model has as much information as required.
This means more focused data to train \ac{ANN} and less refining efforts which translates into faster training times.

\section{Unity Machine Learning Agents Toolkit}
As the previous examples have shown, \ac{ANN} has seen some advances and usages in recent years.
Simulation platforms are one of the many tools that had help in the advancement of \ac{ANN} and \acp{AI} in general. 
Examples of these platforms are \ac{ALE} \cite{Bellemare:2013}, VizDoom \cite{Kempka:2016} and Mujoco \cite{Todorov:2012}.
The existence of these environments have been essential because they provide the means to benchmark and test algorithms. 
Also, these simulation platforms serve not only to enable algorithmic
improvements, but also as a starting point for training models which may be deployed
in the real world. A prime example of this is the work being done to train autonomous robots within
a simulator, and transfer that model to a real-world robot \cite{Andrychowicz:2018}. In
these cases, the simulator provides a safe, controlled, and accelerated training environment.

Pic-a-boo is a game that was developed using the Unity game engine and conveniently Unity provides a good platform for developing \acp{AI}.
This platform is known as \ac{UMLAT}. \ac{UMLAT} is an open source project that enables researchers and developers to create
simulated environments using the Unity Editor and interact with them using a Python API. The
toolkit is built to take full advantage of the properties of the Unity Engine which
makes it a strong research platform \cite{Juliani:2018}.

\ac{UMLAT} provides many resources necessary for creating simulated
environments in Unity. It contains two components. First a \ac{SDK}, which
contains all functionality required to create environments within the Unity Editor and associated C\#
scripts. Second a Python package which enables interfacing from outside of Unity with environments built using the \ac{SDK}.

\ac{UMLAT} presents a high-level \ac{API} for creating \acp{AI} using \acp{ANN}.
This means that designing and implementing an \ac{ANN} is done through configuring a series of parameters exposed by the \ac{API}.
For instance parameters like: number of layers, numbers of units in layers, input format, etc.
\ac{UMLAT} will read these parameters and create the desired modification in the provided basic \ac{ANN} architectures.
To create these \ac{ANN} \ac{UMLAT} uses the TensorFlow python package.
TensorFlow is an open-source software from Google used for data-flow and differentiable programming. 
It is mostly a math library, and one of its main uses is machine learning applications such as neural networks \cite{tensorflowyoutube:2019}.

\ac{UMLAT} provides a set of baseline algorithms implemented with TensorFlow, which can be used for starting the development of novel algorithms. 
The toolkit currently provides an implementation of \ac{PPO}\cite{Schulman:2017}, a state-of-the-art Deep Reinforcement Learning algorithm, 
with the option to extend it using an \ac{ICM}\cite{Pathak:2017} and \ac{LSTM}\cite{Hochreiter:1997}. However, the most relevant of the algorithms for this project is the Behavioral Cloning, 
a simple Imitation Learning algorithm\cite{Hussein:2017} that was implemented to showcase the Unity Editor as a tool for recording expert demonstrations.

Any scene object within Unity can be made into a learning environment. 
To create a learning environment is only necessary to include the \ac{UMLAT}-\ac{SDK} into the Unity project
Once included the \ac{SDK} provides three entities \textbf{agent}, \textbf{brain}, and \textbf{academy}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\textwidth]{./Images/learning_environment_example.png}
    \caption{Block diagram of ML-Agents toolkit example game.}
    \label{fig:UMLAT-block-diagram}
\end{figure}

An \textbf{agent} object represents an actor in the scene that collects observations and carries out actions. The
\textbf{academy} object orchestrates agents and their decision-making processes. The \textbf{brain} asset is
responsible for providing a policy for decision-making for each of its associated agents, a general overview of the toolkit is shown in \Cref{fig:UMLAT-block-diagram}.

Given the fact that Pic-a-boo is developed in Unity, this project will use \ac{UMLAT} due to several reasons:
\begin{itemize}
    \item Having a Behavioral cloning algorithm already implemented speed-ups the development process.
    \item Using \ac{UMLAT} will ensure that the agents can be coupled with the game seamlessly.
    \item The project is maintained by Unity technologies and has an active community which provides long term support.
\end{itemize}

\section{Measuring Artificial Intelligence}

\subsection{Human Tests}
One important aspect of this work will be how to test the final
results. There is no standard way of measuring human behavior. One
good way that has been used before \cite{Khalifa:2016} is to ask a human because humans have the innate ability to recognize other humans.
In the previous mentioned work \cite{Khalifa:2016} the researchers used
this method. The researchers presented a video of the resulting agent to various
people. The video contained
several play-traces of the agents mixed with players. Each participant had
to identify between humans and agents.

This test has a close relationship with the Turing test \cite{Proudfoot:2011} which consist of an
imitation game. The idea of the test is that a machine has to try to pretend
to be a human. The machine should answer questions put to it. It will only pass
if it manages to convince a human that the machine is a human. This
human should not have a considerable knowledge of how machine works \cite{Proudfoot:2011}.
By using this kind of tests we can get an idea of the agent quality.
However, the test will always be a little biased, because it will depend on the
previous knowledge of the participants. Some participants may have knowledge
of the game. This knowledge helps to identify quirks in the agent that a normal
player will not perform. However, a less expert person may not identify the agents. So
previous knowledge is an important factor for these tests.

\subsection{Confusion Matrix}
A more unbiased way of measuring an \ac{AI} will be using a mathematical model.
In \cite{Patterson:2017} the main tool used for evaluating agents is the \textbf{confusion matrix} as seen in \Cref{fig:confusion-matrix}.
For \cite{Patterson:2017} evaluating a model is the process of understanding how good are the predictions the model makes.
A \textbf{confusion matrix} is a table of rows and columns that presents predictions versus actual
outcomes. It is used to understand how it is performing based on giving the correct answer at the right moment.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Confusion_Matrix.png}
    \caption{A confusion matrix diagram}.
    \label{fig:confusion-matrix}
\end{figure}

A binary confusion matrix measures 4 values. 
To better understand this, imagine having an \ac{AI} capable of classifying between humans or an \ac{AI} acting as human the values will be:

\begin{itemize}
    \item \ac{TP}: a true prediction of a positive outcome, a human is classified as a human.
    \item \ac{FP}: a false prediction of a positive outcome, an \ac{AI} is classified as a human.
    \item \ac{TN}: a true prediction of a negative outcome, an \ac{AI} is classified as \ac{AI}.
    \item \ac{FN}: a false prediction of a negative outcome, a human is classified as \ac{AI}.
\end{itemize}

With these values in hand a more detailed analysis on the performance of the model can be made by calculating the following four relationships:

\begin{itemize}
    \item Precision: The degree to which repeated measurements under the same conditions give us the same results.
    \[P = TP / (TP + FP)\]
    \item Recall: how often we classify an input record as the positive class and it is the correct classification.
    \[R = TP/ (TP + FN)\]
    \item Specificity: how often we classify an input record as the negative class and it is the correct classification.
    \[R = TN/ (TN + FN)\]
    \item Accuracy: Accuracy is the degree of closeness of measurements of a quantity to that quantity’s true value.
    \[A =(TP + TN) / (TOTAL)\]
    \item F1 Score: is the harmonic mean of both the precision and recall measures can be used as an overall score on how well the model is
    performing.
    \[F1S = (2*(P * R))/(P + R) = 2TP / (2TP + FP + FN)\]
\end{itemize}

\subsection{Multi Class Confusion Matrix}

The goal of a great number of \acp{ANN} models is to learn to map
a given input to a corresponding known output.
For \textbf{prediction} tasks, the output comes in the form of a single
label. For \textbf{regression} tasks, it is a single value. \acp{ANN} are able to solve
these simple single-output problems like binary classification with ease. 
For instance \acp{ANN} are able to filter spam in an email system or label images.

However, these single output models are not
coping well with the increasing needs of today’s complex
decision-making. As a result, there is a pressing need for
new paradigms. Here, multi-output learning
has emerged as a solution. The aim is to simultaneously
predict multiple outputs given an input, which means it is
possible to solve far more complex decision-making problems.

Pic-a-boo player actions can't be solved with a binary output alone.
For instance moving in the game requires 5 different outputs up, down, left, right and neutral.
The problem lies in how these outputs are to be measured. 
A binary confusion matrix is not enough to solve the problem.
To solve the measuring problem this work is going to use a multi-class confusion matrix\cite{Shmueli:2019}.

Binary classification problems usually focus on a positive and a negative class which must be detected. 
Instead, in a multi-class classification problem, we need to categorize outputs into 1 of N different classes.

For example, we can use an \ac{AI} trained to predict player movements between left, right or remain neutral.
A multi-class confusion matrix will then measure n by n values for this case n=3 that would mean 9 measures which can be observed in
\Cref{fig:multi-confusion-matrix}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/multi_confusion_matrix.png}
    \caption{A multi confusion matrix example using dogs, cats and hens}.
    \label{fig:multi-confusion-matrix}
\end{figure}

With these values an analysis similar to the binary confusion matrix can be made by calculating 3 of the previous relationships as can be seen in 
\Cref{fig:multi-confusion-matrix-metrics}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/confusion_matrix_metrics.png}
    \caption{Multi confusion matrix metrics}.
    \label{fig:multi-confusion-matrix-metrics}
\end{figure}

After calculating each one of these we can them calculate the mean recall precision and mean f1-score for the model as:

\begin{itemize}
    \item Mean Precision:
    \[MP = (AP + BP + CP)/ 3 \]
    \item Mean Recall:
    \[MR = (AR + BR + CR)/ 3 \]
    \item Mean F1-Score:
    \[MF1S = (AF1S + BF2S + CBF3S)/ 3 \]
\end{itemize}

The these values are also known as macro-precision, macro-recall and macro-F1-Score, in this work they will be known as the macro-metrics of a confusion matrix.
Nonetheless, there is a second set of metrics that can be calculated.
These metrics are what we are going to call the micro-metrics, the micro-metrics do not analyze the matrix by class but instead as a whole. 
However, there is a particularity, in a multi-class confusion matrix when analyzing the whole matrix a \ac{TP} and \ac{TN} have the same meaning both are correct predictions. 
\ac{FP} and \ac{FN} also have the same meaning both are incorrect predictions so that means:

\begin{itemize}
    \item Micro-Precision:
    \[MP = (TP) / (TP + FN) = (AA + BB + CC)/(AA + AB + AC + BA + BB + BC + CA + CB + CC)\]
    \item Micro-Recall:
    \[MP = (TP) / (TN + FP) = (AA + BB + CC)/(AA + AB + AC + BA + BB + BC + CA + CB + CC)\]
    \item Accuracy: Accuracy is the relationship between good predictions and the total amount of predictions
    \[A = (MATCHES)/(TOTAL) = (AA + BB + CC)/(AA + AB + AC + BA + BB + BC + CA + CB + CC)\]
    \item Micro-F1 Score: since micro-precision=micro-recall, they are also equal to their harmonic mean
    \[(AA + BB + CC) / (AA + AB + AC + BA + BB + BC + CA + CB + CC)\]
\end{itemize}


In addition to theses four parameters calculated from the confusion matrix, other metrics exist for instance the hamming loss\cite{Xu:2019}, 
commonly used with strings. The hamming loss computes the average difference between the predicted and actual output.
These differences are calculated by counting the number of different outputs in a set. Now going back to the previous example of 
an \ac{AI} trained to predict player movements between left, right or remain neutral, imagine we are comparing two sequences of actions:

\begin{itemize}
    \item Player: “left, right, \textcolor{red}{left, left}, right, neutral, neutral”
    \item AI prediction: “left, right, \textcolor{violet}{right, right}, right, neutral, neutral”
\end{itemize}

Between these sequences there are 2 differences. 
D will be the sum of all the differences found and N the number of comparisons.
\[HL = D / N\]
So for this example, the hamming loss will be:
\[HL = 2 / 7 = 0.28\]
Normally hamming loss is used for string however the output of the model can be treated as a string, so hamming loss can be applied.

Another related metric that can be calculated is the percentage of perfect predictions of the model.
Meaning that the difference between both actions is 0, For this project this metric will be called \textbf{Perfect match}.
Z will be the number of zero matches and N will be the total number of sequences.
\[PM = Z / N\]
Going back to the movement predicting \ac{AI} suppose we have the following set:

\begin{itemize}
    \item First Player movement: “left, right, \textcolor{red}{left, left}, right, neutral”
    \item First AI prediction: “left, right, \textcolor{violet}{right, right}, right, neutral”
    \item Second Player movement: “left, left, neutral, right, right, left”
    \item Second AI prediction: “left, left, neutral, right, right, left”
    \item Third Player movement: “left, right, neutral, right, \textcolor{red}{right}, left”
    \item Third AI prediction: “left, right, neutral, right, \textcolor{red}{left}, left”
\end{itemize}

For this case, only the second prediction has a difference of 0 so the \textbf{Perfect match} will be:
\[PM = 1 / 3 = 0.33..\]

With these metrics the models now can be correctly evaluated to check the quality and differences of the generated clones.

\section{Closing Remarks}

To close this chapter a recapitulation can be made with the elements necessary to progress. 
This project will use the \textbf{clone} model that encompasses the creation of an \ac{AI} by extracting the patterns from human data.
To extract these patterns, an \ac{ANN} will be used which is an algorithm that matches a set of inputs to a set of outputs.
The \ac{ANN} will be using \textbf{symbolic} inputs which are a representation system that has a direct meaning without requiring previous interpretation.
The project will be implemented using \ac{UMLAT} to speed-up development and to ease the coupling with Pic-a-boo. 
Finally, the project will be measured using both a \textbf{Confusion Matrix} and its various associated metrics and recognition test with human subjects.

%Cras dictum. Maecenas ut turpis. In vitae erat ac orci dignissim eleifend. Nunc quis justo. Sed vel ipsum in purus tincidunt pharetra \cite{MacAulay:2005fk}. Sed pulvinar, felis id consectetuer malesuada, enim nisl mattis elit, a facilisis tortor nibh quis leo. Sed augue lacus, pretium vitae, molestie eget, rhoncus quis, elit \cite{Schwarz:2007lr}. Donec in augue. Fusce orci wisi, ornare id, mollis vel, lacinia vel, massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas..

%Sed pulvinar, \enquote{felis id consectetuer} malesuada, enim nisl mattis elit, a facilisis tortor nibh quis leo \Cref{tab:streamingtech}.

%\begin{table}[htb]
%\centering
%\normalsize
%{\footnotesize
%    \caption{Streaming Technologies Comparison}
%    \label{tab:streamingtech}
%    \begin{tabular}{ | c | c | c | c |}
%    \hline
%    & Dynamic & Smooth & HLS\\
%    & Streaming & Streaming & \\ \hline \hline
%
%    Streaming Protocol & RTMP & HTTP & HTTP \\
%    %\textbf{Protocol} & & & \\ 
%    \hline
%    
%    Video Codec & H.264, VP6 & H.264 & H.264 \\ 
%    %\textbf{Codec} & &  & \\ 
%    \hline
%    
%    Audio Codec & AAC, MP3 & WMA, AAC & AAC, MP3  \\
%    %\textbf{Codec} & & & \\ 
%    \hline
%    
%    Container Format & MP4, FLV, & MP4 & MPEG2-TS \\
%    %\textbf{Format} & F4V & & \\ 
%    \hline
%    
%     iOS & NO & YES & YES \\ \hline
%     
%    Android & NO & YES & YES \\ \hline
%    
%    \end{tabular}
%    }
%\end{table} 
%
%Suspendisse vestibulum dignissim quam. Integer vel augue. Phasellus nulla purus, interdum ac, venenatis non, varius rutrum, leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas \cite{RFC-VP8}. Duis a eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce magna mi, porttitor quis, convallis eget, sodales ac, urna \cite{Chiang:2011fk}. \textcolor{violet}{\Cref{tab:spreadtb} illustrates the use of a Spreadsheet-like table producing calculations by columns and by lines (observe the code).} 
%
%\begin{table}[htb]
%\centering
%    \caption{A nice Spreadsheet using package ``spreadtab''. Notice the calculations.}
%    \label{tab:spreadtb}
%\begin{spreadtab}{{tabular}{rr|r}} 
%22       & 54       & a1+b1 \\
%43       & 65       & a2+b2 \\ 
%49       & 37       & a3+b3 \\
%\hline
%a1+a2+a3 & b1+b2+b3 & a4+b4
%\end{spreadtab}
%\end{table} 
%% #############################################################################
%\section{Cras lobortis tempor velit}
%Nunc tincidunt convallis tortor. Duis eros mi, dictum vel, fringilla sit amet, fermentum id, sem. Phasellus nunc enim, faucibus ut, laoreet in, consequat id, metus. Vivamus dignissim \cite{Moscoso:2011fk}. \textcolor{violet}{\Cref{tab:comp_arch} is automatically compressed to fit text width. You can use \url{https://www.tablesgenerator.com} to produce these tables, and then copy the \LaTeX\space code generated to paste in the document.}
%
%
%\begin{table}[h]
%\centering
%\caption{Comparison between today's and target Architectures of Telcos}
%\label{tab:comp_arch}
%\resizebox{\textwidth}{!}{%
%\begin{tabular}{|
%>{\columncolor[HTML]{ECF4FF}}l |l|
%>{\columncolor[HTML]{E2FFC9}}l |l|}
%\hline
%\multicolumn{2}{|c|}{\cellcolor[HTML]{ECF4FF}Today}                                                                                                             & \multicolumn{2}{c|}{\cellcolor[HTML]{E2FFC9}Target}                                                                                  \\ \hline
%Rigid     & \begin{tabular}[c]{@{}l@{}}Each evolutionary requirement involves \\ development of multiple components, \\ interfaces, platforms,etc.\end{tabular} & Flexible       & \begin{tabular}[c]{@{}l@{}}It is possible to modify or add new\\ functionalities rapidly.\end{tabular}              \\ \hline
%Slow      & \begin{tabular}[c]{@{}l@{}}Development of a new application takes \\ months or years.\end{tabular}                                                  & Fast           & \begin{tabular}[c]{@{}l@{}}Development of a new application takes \\ weeks instead of months or years.\end{tabular} \\ \hline
%Closed    & \begin{tabular}[c]{@{}l@{}}Limited integration with external\\ environments.\end{tabular}                                                           & Open           & \begin{tabular}[c]{@{}l@{}}It is simple to integrate internal,\\ applications with external entities.\end{tabular}  \\ \hline
%Complex   & \begin{tabular}[c]{@{}l@{}}Heterogeneous technologies, obsolescence, \\ lack,of standards, high redundancy.\end{tabular}                            & Standardised   & Use of homogeneous architectural models.                                                                            \\ \hline
%Expensive & \begin{tabular}[c]{@{}l@{}}High Capex (for new service development) \\ and,high,Opex (to ensure running of IT).\end{tabular}                        & Cost-Effective & Capex and Opex are optimised.                                                                                       \\ \hline
%\end{tabular}
%}
%\end{table}
%
%Cras lobortis tempor velit. Phasellus nec diam ac nisl lacinia tristique. Nullam nec metus id mi dictum dignissim. Nullam quis wisi non sem lobortis condimentum. Phasellus pulvinar, nulla non aliquam eleifend, tortor wisi scelerisque felis, in sollicitudin arcu ante lacinia leo.