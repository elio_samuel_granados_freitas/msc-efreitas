% #############################################################################
% This is Chapter 5
% !TEX root = ../main.tex
% #############################################################################
% Change the Name of the Chapter i the following line
\fancychapter{Results}
\cleardoublepage
% The following line allows to ref this chapter
\label{chap:results}

\section{Chapter description}
This chapter showcases the results of the several measures done through the development of the project.
The results are separated into two sections.
The first section showcases the values that lead to the decision of stopping the learning process at 10000 iterations with 2 layers.
The next section showcases the results of the cloning capability of the agents.

Throughout the chapter the confusion matrices results are grouped into three related sets.
First the taunt and flash results values that correspond to actions that affected visibility and whose occurrence in the data set were rare.
Next the Confusion matrices for navigation composed of the horizontal and vertical actions, actions that affected the player position and whose occurrence was common.
Finally, the global analysis parameters hamming loss, perfect match and its complement imperfect match, values that were calculated by counting all the predictions made in a complete game session.

All the following graph were generated from the results that can be found in \Cref{chapter:appendixA},
additionally the legends for them can be seen in \Cref{tab:legendGraph}.

\begin{table}[!htb]
    \centering
    \csvautotabular{./tables_and_code/GraphLegends.csv}
    \caption{Graph legends}
    \label{tab:legendGraph}
  \end{table}

\section{Learning optimization results}

\subsection{Iteration stopping point}

For these measures, three games were recorded and then inputted into the \acp{ANN} model manually across various training stages,
to be specific 10, 3000, 5000, 10000, 20000.
The values present in the following graph are the average of these games.
These tests were done with the objective of understanding at what point the agent achieved its best performance to stop the training process.
The model used was the 2 layers model presented in \Cref{fig:Feed-forward-Neural-Network-Topology}.

\subsubsection{Confusion matrices for flash and taunt}

The metrics progression for the flash action can be seen in \Cref{fig:flashActionProgress} and for the taunt action in \Cref{fig:tauntActionProgress}
Specificity starts high and maintains itself high. 
Both the flash and taunt are actions which rarely occurs in the game,
players are normally moving and only flash or taunt when is necessary.
Specificity measures the rate of negatives recognition, 
so the high values are caused by the low occurrence of the flash or taunt.
However, the fact that the lines remain around the same values means 
that there is no large difference between a new agent and an untrained agent regarding when not to flash or taunt.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Flash_action_by_iterations.png}
    \caption{Flash action progress.}
    \label{fig:flashActionProgress}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Taunt_action_by_iterations.png}
    \caption{Taunt action progress.}
    \label{fig:tauntActionProgress}
\end{figure}

\FloatBarrier

Accuracy starts low and rapidly increases showing that the agent is improving the recognition of events.
One important aspect to notice is that there are more \ac{TN} events than \ac{TP} as you can see in \Cref{tab:flashTableG1}, \Cref{tab:flashTableG2}, \Cref{tab:flashTableG3} and \Cref{tab:TauntTableG1}, \Cref{tab:TauntTableG2} or \Cref{tab:TauntTableG3}.
This means that the accuracy is going higher not because the agent is improving in using correctly the flash or taunt but instead the agents avoid using it.
Meaning the agent doesn't understand how to use the flash correctly to win the game or the taunt to bait the enemies. 
It just predicts that it shouldn't use it because it's the most common occurrence, getting some correct predictions because it's common not to use a flash or a taunt.
\\
As for precision, it starts high and then decreases, mirroring the accuracy.
This is because precision measures the prediction of positive values, the flashing and taunting actions are used more frequently in untrained agents.
Trained agents begin to understand that the both actions aren't supposed to be used all the time because it's the most common event. 
This lowers the precision because there are few flashing or taunting events and for those few events the prediction is incorrect. 
In other words the untrained agents flashes or taunts frequently guessing the prediction by just trying.
\\
Recall for the flash follows a similar path as the precision differing at the beginning, 
because at the beginning the agent doesn't known when to hold the actions increasing the \ac{FP}.
\\
Finally, the F1Score which is a harmonic mean between precision and recall.
The F1Score remains lows through the iterations. It shows some progress however it goes up and down almost like a sine function for the flash and a cosine for the taunt.
F1Score shows the overall quality of the agent and its results express that the agent does not understand when is the right opportunity to use neither action.
The only thing that it is learning is to avoid using taunts or flashes.
\\
The taunt presents lower values in general compared to the flash but each parameter follows a similar trend.
This could be because the taunt action is a difficult action to predict because it depends on the ability of a player to find themselves in the map.
Human players use taunt normally when they feel lost which makes it difficult to determine a good opportunity for when to taunt. 
In contrast, the best opportunity for a flash is when there are players nearby.
\\
Nonetheless, the agents are unable to learn how to taunt or flash because both events are rare compared to the navigation events.
The erratic behavior for both taunt and flash can be seen clearly while playing, there aren't any patterns in the execution of these actions, 
and if an agent catches a player with the flash, it was most likely by accident.
These predictions are so chaotic that we could substitute them 
for a simple random algorithm and the result would be similar to the agent constructed in this project.
In fact a heuristic approach could be a more robust solution for the flash and taunt action.
\\
It is important to notice that the effect of these two actions in the cloned subjects perception of their clone is not apparent as can be seen in \Cref{tab:SurveyR1}, \Cref{tab:SurveyR2}, \Cref{tab:SurveyR3}.
No one mentioned flashes or taunts as the reasoning behind a selection, but various subjects mentioned positioning.
So given the fact that the importance of the taunt and flash in clone perception is low and that the predictions are chaotic, 
these actions weren't a factor in choosing the 10000 mark.
However, it is worth noticing that the flash has a peak at 10000,but the same cannot be said for the taunt.

\subsubsection{Confusion matrices for navigation}

Now continuing with the navigation.
For horizontal action the progress is shown in \Cref{fig:horizontalActionProgress} and for the
vertical progress in \Cref{fig:verticalActionProgress}.
\\
In this case all the metrics are near or beyond the 0.70 mark.
This means that the agents have a good understanding on how to navigate both horizontally and vertically compared to the taunt and flash actions.
Horizontal and vertical navigation are the most common events in Pic-a-boo. However, horizontal navigation is more common because 
the map has more width than height forcing the players to navigate more horizontally than vertically.
The abundance of events compared to the flash and taunt events reflects itself in the overall performance of the agent while navigating.
Having more events helped the agent to learn how to navigate with consistency.
\\
Any point between 5000 or 10000 could have served as a stopping point for simulation
The agent navigation is at its best in this range and decreases if the agent goes beyond that.
The 10000 point was chosen because with 5000 the cloned subjects were still getting comfortable with the training experience.
Referring back to the human perception, the most mentioned aspect for identifying a clone is also the navigation as seen in \Cref{tab:SurveyR1}, \Cref{tab:SurveyR2}, \Cref{tab:SurveyR3}.
This enforces even more the reasoning behind the selection of the 10000 mark as the stopping point favoring navigation over anything else.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Horizontal_action_by_iterations.png}
    \caption{Horizontal action progress.}
    \label{fig:horizontalActionProgress}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Vertical_action_by_iterations.png}
    \caption{Vertical action progress.}
    \label{fig:verticalActionProgress}
\end{figure}

\FloatBarrier

\subsubsection{Global analysis parameters}

For the global metrics the progress can be seen at \Cref{fig:GlobalMetricsProgress}.
Beginning by the hamming loss, it dictates how many errors there are in the prediction,
it begins high beyond the 2.0 mark and begins to decrease.
So initially the agents have in average more than 2 errors, 
there are only 4 actions so that means the agent if failing predictions constantly.
The hamming loss stabilizes near 1.0 around the iteration 5000.
Following training the agent commonly fails only 1 predicted action in average.
By taking into account the previous graphs it can be assumed that this failed prediction is caused by failing to predict a flash or a taunt.

For the perfect and imperfect match cases the graph mirror each other because these values are complementary and as the agent trains, it is able to make more perfect predictions.
However, the agent is unable to make more perfect predictions than imperfects.
Again it can be assumed that the predictions are failing because of the difficulty in predicting the flash or taunt.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Global_metrics_by_iterations.png}
    \caption{Global metrics progress.}
    \label{fig:GlobalMetricsProgress}
\end{figure}

\FloatBarrier

\subsubsection{Closing remarks}
To conclude, the main idea to extract from here is that for navigation the best performance is achieved in the range between 5000 and 10000 iterations. 
Having more than 10000 iterations does not improve the agent performance instead it may decrease it.
As for the taunt and flash, no matter how many iterations are done the performance
is never good. This is because of the scarcity of the events and the difficulty of determining when to use these actions.
\newpage

\subsection{Optimal Layer count}
For these measures the same three games were inputted into three different \acp{ANN} that were similar to the one in \Cref{fig:Feed-forward-Neural-Network-Topology},
The main difference being that the hidden layers value was increased from 2 to 4 and 6.
All the models were trained for the exact same amount of iterations 10000.
The following graphs are the average of the recorded games.
The idea was to understand if the performance could be improved by adding more layers.

\subsubsection{Confusion matrices for flash and taunt}

In \Cref{fig:flashActionLayer}, \Cref{fig:flashActionLayerCandle},  \Cref{fig:tauntActionLayers} and \Cref{fig:tauntActionLayersCandle} the comparison between the performance of the agents when increasing the number of layers in the model can be seen.
Both specificity and accuracy have close values when compared between layers but the 2 layer models has the overall higher values.
The biggest difference is present in the f1Score, precision, and recall metrics.
For the flash the 2 layer model is clearly superior when compared to the other ones just by checking these three metrics.
The same cannot be said for the taunt, for that case the 4 layer model is the superior one.
Taunt is the only action where the 2 layer model is not the superior one.
However, the impact of the taunt action in the game is minimal, there is no clear moment when it should be used.
As said before taunt is used normally when human players are feeling lost in the game.
However, in general the agents are still bad at understanding when to flash or taunt, increasing layers did not improve the performance in any significant way.
All these models were trained for 10000 iterations.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Flash_action_by_layers.png}
    \caption{Flash action by layers average.}
    \label{fig:flashActionLayer}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Flash_action_by_layers_candle.png}
    \caption{Flash action by layers box plot.}
    \label{fig:flashActionLayerCandle}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Taunt_action_by_layers.png}
    \caption{Taunt action by layers average.}
    \label{fig:tauntActionLayers}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Taunt_action_by_layers_candle.png}
    \caption{Taunt action by layers box plot.}
    \label{fig:tauntActionLayersCandle}
\end{figure}

\FloatBarrier

\subsubsection{Confusion matrices for navigation}

For the navigation, the comparison is shown in \Cref{fig:horizontalActionLayers}, \Cref{fig:horizontalActionLayersCandle}, \Cref{fig:verticalActionLayers} and \Cref{fig:verticalActionLayersCandle}.
In this comparison all the metrics are very close to each other, near or beyond the 0.70 mark.
So no matter how many layers were used the agents still navigated more or less the same.
However, the 2 layer model also has the biggest performance peaks compared to the other ones.
Nonetheless, the difference is not enough to be significant especially when compared to the 6 layer models.
Being marginally the highest performing model and also the fastest to train the 2 layer was selected to create the agents.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Horizontal_action_by_layers.png}
    \caption{Horizontal action by layers average.}
    \label{fig:horizontalActionLayers}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Horizontal_action_by_layers_candle.png}
    \caption{Horizontal action by layers box plot.}
    \label{fig:horizontalActionLayersCandle}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Vertical_action_by_layers.png}
    \caption{Vertical action by layers average.}
    \label{fig:verticalActionLayers}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Vertical_action_by_layers_candle.png}
    \caption{Vertical action by layers box plot.}
    \label{fig:verticalActionLayersCandle}
\end{figure}

\FloatBarrier

\subsubsection{Global analysis parameters}

The global metrics comparisons can be seen at \Cref{fig:GlobalMetricsLayers} and \Cref{fig:GlobalMetricsLayersCandle}.
In this case the values are again very close.
The 2 layer model wins in the hamming loss because is the lowest.
However, for the perfect match metric the 4 layer models is a bit better.
Still, this difference did not justify increasing the quantity of layers.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Global_metrics_by_layers.png}
    \caption{Global metrics by layers average.}
    \label{fig:GlobalMetricsLayers}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Global_metrics_by_layers_candle.png}
    \caption{Global metrics by layers box plot.}
    \label{fig:GlobalMetricsLayersCandle}
\end{figure}

\FloatBarrier

\subsubsection{Closing remarks}
The idea that can be extracted from these measures is simple, increasing layers does not modify significantly the performance of the agent.
Given this fact the layer quantity was set to 2 because the fewer layers the model had, the faster it trained.

\section{Imitation quality results}

For this test, 6 extra agents were trained by different human subjects, the agents were code named \textbf{AR, BH, MF, TS, VC and VG}.
the seventh agent code named \textbf{EF} is the agent trained by this project main researcher.
Each subject trained an \ac{ANN} by playing against 3 agents continuously for 10000 iterations which took around 20 minutes.
In the first part all the agents try to predict the main researcher game.
For the second part the cloned subjects were asked to identify their clones inside a match while three other different clones were present.

\subsection{Cross agent comparisons}

For this subsection, previously recorded games were inputted into each of the 7 agents.
These games were all played and recorded by this project main researcher as reference, and were different from the games used for training the agents.
The objective of this test was to check whether a clone was capable of predicting another person game.
If clones are truly different they should have different predictions, because of the different play-style of each person.
Furthermore, someone's clone should be the best predictor of that person's clone.\\
In this case the new agents namely \textbf{AR, BH, MF, TS, VC and VG} were trying to predict the main researcher's games. The \textbf{EF} agent which is the researcher's clone served as a reference for the measures.
To be able to support the previous statements, \textbf{AR, BH, MF, TS, VC and VG} predictions should be different from each other, and \textbf{EF} should be the overall best predictor.

\subsubsection{Confusion matrices for flash and taunt}

We will begin with the flash and taunt action found in \Cref{fig:flashActionBrain}, \Cref{fig:flashActionBrainCandle}, \Cref{fig:tauntActionBrain} and \Cref{fig:tauntActionBrainCandle}.
The graphs show differences between each clone showcasing that some clones are better predictors.
Nonetheless, the graph for these actions are very spread and chaotic as shown in \Cref{fig:flashActionBrainCandle} and \Cref{fig:tauntActionBrainCandle}.
In the flash graph the \textbf{EF} clone is one of the best predictors having the highest average and peaks but with medians close to \textbf{AR}.
Both the flash and taunt graph validate the idea that clones are indeed different,
However the idea that the clone of one person is the best at predicting that person game may be challenged if we check the taunt graph.
\\
For the taunt case \textbf{EF} predicting \textbf{EF} is one of the worst predictions.
It is important to remember that the taunt action is not the most consistent action as shown in \Cref{fig:tauntActionBrainCandle} the taunt values are very spread.
For example the previous iteration graph presented in \Cref{fig:tauntActionProgress} suggested that an agent with no training was capable of predicting the taunt better than a trained agent.
So the explanation for this result where \textbf{MF} 
is predicting \textbf{EF} is the same as before, the \textbf{MF} is trying to taunt much more than the \textbf{EF} brain.
This explanation can be further supported by the \textbf{MF}'s accuracy which is lower than \textbf{EF}'s,
the best values for accuracy in \textbf{MF} are the worst for \textbf{EF} as seen in \Cref{fig:tauntActionBrainCandle}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Flash_action_compared_between_brains.png}
    \caption{Flash action by brains average.}
    \label{fig:flashActionBrain}
 \end{figure}

 \begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{./Images/Graph/Flash_action_compared_between_brains_candle.png}
    \caption{Flash action by brains box plot.}
    \label{fig:flashActionBrainCandle}
 \end{figure}

 \begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Taunt_action_compared_between_brains.png}
    \caption{Taunt action by brains average.}
    \label{fig:tauntActionBrain}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{./Images/Graph/Taunt_action_compared_between_brains_candle.png}
    \caption{Taunt action by brains box plot.}
    \label{fig:tauntActionBrainCandle}
\end{figure}

\FloatBarrier


\subsubsection{Confusion matrices for navigation}

For the navigation, the comparisons are shown in \Cref{fig:horizontalActionBrain}, \Cref{fig:horizontalActionBrainCandle}, \Cref{fig:verticalActionBrain} and \Cref{fig:verticalActionBrainCandle}.
The values are close between each agent, near or beyond the 0.70 mark.
But there are clear differences between each other. 
This supports the idea that each clone is different.
For the idea that the clone of the person is the best at predicting that person action.
\textbf{EF} predicting \textbf{EF} is one of the best performing brain as shown by its average being among the highest, 
However is surpassed by \textbf{AR}, \textbf{BH}, \textbf{MF} in some peaks as shown in \Cref{fig:horizontalActionBrainCandle} and \Cref{fig:verticalActionBrainCandle}.
Nonetheless, \textbf{EF}'s values are more consistent than \textbf{AR}, \textbf{BH} and \textbf{MF}.
Meaning that while other brains are able to predict \textbf{EF} their values are more spread supporting the idea that \textbf{EF} is the best predictor of \textbf{EF}.
However, It is important to remark that overall differences are not big enough and \textbf{EF} is barely ahead of others.
Suggesting that while some brains might be better predictors than others it is difficult to differentiate between each clone navigation pattern.
The navigation patterns similarities could be explained by the influences coming from the map layout, the map is static and has more width than height, 
forcing the players to move horizontally most of the time to reach their objectives.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Horizontal_action_compared_between_brains.png}
    \caption{Horizontal action by brains average.}
    \label{fig:horizontalActionBrain}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{./Images/Graph/Horizontal_action_compared_between_brains_candle.png}
    \caption{Horizontal action by brains box plot.}
    \label{fig:horizontalActionBrainCandle}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Vertical_action_compared_between_brains.png}
    \caption{Vertical action by brains average.}
    \label{fig:verticalActionBrain}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{./Images/Graph/Vertical_action_compared_between_brains_candle.png}
    \caption{Vertical action by brains box plot.}
    \label{fig:verticalActionBrainCandle}
\end{figure}

\FloatBarrier

\subsubsection{Global analysis parameters}

The global metrics comparisons can be seen in \Cref{fig:GlobalMetricsBrain} and \Cref{fig:GlobalMetricsBrainCandle}.
For this case, \textbf{BH} is the best brain, compared to \textbf{EF} in all metrics.
Both hamming loss and imperfect matches are lower than \textbf{EF}.
The \textbf{BH} brain was one of the closest to the \textbf{EF} brain in the previous actions.
So it is no surprise that it is able to make good predictions. 
Nonetheless, the difference between \textbf{EF} and \textbf{BH} prediction is not big.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Global_metrics_by_brain.png}
    \caption{Global metrics by brain average.}
    \label{fig:GlobalMetricsBrain}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\textwidth]{./Images/Graph/Global_metrics_by_brain_candle.png}
    \caption{Global metrics by brain box plot.}
    \label{fig:GlobalMetricsBrainCandle}
\end{figure}
\FloatBarrier

\subsubsection{Closing remarks}
The main idea to extract from these tests is that the clones are very similar.
To the point that a brain not trained by the user is able to predict the user's games, 
suggesting that there is no different-play style. However, when taking into account all the tests as a whole,
the \textbf{EF} brain is one of the best performing and most consistent brains. For the cases where it is not the best, it is one of the best.
In comparison, the brains that were able to surpass \textbf{EF} in one type of actions is not able to surpass it in another.
For instance \textbf{MF} surpass \textbf{EF} metrics in some values of taunt and vertical actions but falls in the horizontal and global metrics.
So the overall best and most consistent predictor of a person's game is its own clone.

\subsection{Survey}

In this section, the results from the questionnaires are reviewed.
The idea was to support the evidence found through the quantitative tests done previously.
To that goal the 6 previously cloned subjects were asked to recognize themselves in three different games.
A fourth game was also presented as an example. 
The fourth game has all the clones identified to help the subjects understand the idea of the survey and the patterns.

\subsubsection{Survey recognition results}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Survey_recognition_result.png}
    \caption{Survey recognition result.}
    \label{fig:reconResult}
\end{figure}

In \Cref{fig:reconResult}, the total results of the queries can be seen.
There were a total of 18 recognition, 3 games each person multiplied by 6 subjects. Of those 18, 10 were correct and 8 were incorrect.
Meaning that barely more than half of the recognition attempts were correct.
However, this value is better than if answer were chosen randomly which would give a 25\% chance of answering correctly or 33\% if the control agent is discounted.
This may seem to validate the idea than the agents are indeed playing differently or at least there is something that can be recognized.
However, being so close also means that the difference is not enough to be distinguished consistently.

\subsubsection{Recognition confidence}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./Images/Graph/Recognition_confidence.png}
    \caption{Survey confidence result.}
    \label{fig:reconConfidence}
\end{figure}

In the survey, the following affirmation was present: “I am confident in my choice”, 
the subjects could answer either, “Strongly Agree”, “Agree”, “Partially Agree”, “Disagree”. 
The idea was to understand the confidence of the subjects while answering. 
This is because they only have 4 choices, meaning that they had a certain probability of answering correctly by chance.
The graph shown in \Cref{fig:reconConfidence} shows that the subjects were in doubt.
And that doubt was reflected in 
the recognition attempt because both “Agree” and “Partially Agree” successful and failed recognition are equal.
However, only one of the subjects completely disagreed and that subject successfully identified his clone. 
Which shows that even in doubt the subjects perceived some differences or quirks in the clones that helped them differentiate between the clones, 
but not enough to for them to be sure. 
It is important to remark that the subjects were not experienced with the game in general 
nor with the clones resulting behaviors apart from watching the clones in videos, which may impact in the confidence of answers.
As said before previous knowledge is an important factor in recognizing an \ac{AI}.

\subsubsection{Recognition reasoning}

To better understand the reasoning behind the choices of the subjects the survey asked to explain why the selection was made.
The results can be seen in \Cref{section:Survey}.
One common explanation found was that people compared the clones presented in the games to the demo one.
This suggests that rather than identifying themselves, the subjects were identifying the pattern the clone had.
However, there are some answers that reveal that part of the subjects were identifying their own quirks for instance
“Seems a bit erratic at first.. just like me!” and “I was often on the corners”.
This again enforces the idea that clones are indeed different, however the differences are not easily perceived.

\section{Closing remarks}
The results shown throughout this chapter lead to the conclusion that 
this project successfully created agents capable of playing 
Pic-a-boo. The agents were created by imitating the playing patterns of human subjects.
However, agents are not able to reproduce all the actions successfully. 
The agents are only able to navigate the map and can't use the flash or taunt actions correctly.
Alongside this, the navigational patterns produced by different agents are quite similar, something 
players could notice because even the cloned subject had difficulty identifying their own clones.

% Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros. 
% % #############################################################################
% \section{Maecenas vitae nulla consequat} 
% Aliquam aliquet, est a ullamcorper condimentum, tellus nulla fringilla elit, a iaculis nulla turpis sed wisi. Fusce volutpat. Etiam sodales ante id nunc. Proin ornare dignissim lacus. Nunc porttitor nunc a sem. Sed sollicitudin velit eu magna. Aliquam erat volutpat. Vivamus ornare est non wisi. Proin vel quam. Vivamus egestas. Nunc tempor diam vehicula mauris. Nullam sapien eros \Cref{fig:test_env}, facilisis vel, eleifend non, auctor dapibus, pede.
% 
% \begin{figure}[h]
% \centering
% \includegraphics[width=0.8\textwidth]{./Images/test_env}
% \caption{Test Environment}
% \label{fig:test_env}
% \end{figure}
% 
% Aliquam aliquet, est a ullamcorper condimentum, tellus nulla fringilla elit, a iaculis nulla turpis sed wisi. Fusce volutpat. Etiam sodales ante id nunc. Proin ornare dignissim lacus. Nunc porttitor nunc a sem. Sed sollicitudin velit eu magna. Aliquam erat volutpat. Vivamus egestas. Nunc tempor diam vehicula mauris. Nullam sapien eros, facilisis vel, eleifend non, auctor dapibus, pede \Cref{tab:network_profiles} used in the tests. The Network Link Conditioner allows to force/simulate fluctuations in fixed network segments.
% 
% \begin{table}[htb]
% \centering
% \normalsize
%     \caption{Network Link Conditioner Profiles}
%     \label{tab:network_profiles}
% {\footnotesize
%     \begin{tabular}{ | c | c | c | c | }
%     \hline 
%     \textbf{Network Profile}	& \textbf{Bandwidth} & \textbf{Packets Droped} & \textbf{Delay}\\ \hline \hline
%     Wifi  & 40 mbps  &  0\%  &   1 ms \\ \hline
%     3G  & 780 kbps  &  0\%  &   100 ms \\ \hline 
%     Edge  & 240 kbps  &  0\%  &   400 ms \\ \hline
%     \end{tabular}
%     }
% \end{table}
% 
% Aliquam aliquet, est a ullamcorper condimentum, tellus nulla fringilla elit, a iaculis nulla turpis sed wisi. Fusce volutpat. Etiam sodales ante id nunc. Proin ornare dignissim lacus. Nunc porttitor nunc a sem. Sed sollicitudin velit eu magna. Aliquam erat volutpat. Vivamus ornare est non wisi. Proin vel quam. Vivamus egestas. Nunc tempor diam vehicula mauris. Nullam sapien eros, facilisis vel, eleifend non, auctor dapibus, pede.
% % #############################################################################
% \section{Proin ornare dignissim lacus}
% Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis.
% 
% Et ``optimistic'' nulla dui purus, eleifend vel, consequat non, dictum porta, nulla. Duis ante mi, laoreet ut, commodo eleifend, cursus nec, lorem. Aenean eu est. Etiam imperdiet turpis. Praesent nec augue. Curabitur ligula quam, rutrum id, tempor sed, consequat ac, dui $G_j$, nec ligula et lorem consequat ullamcorper $p$ ut mauris eu mi mollis luctus $j$, porttitor ut, \Cref{unchoke_gain}, uctus posuere justo:
% 
% \begin{description}
%   \item[$N_j$] Is the number of times peer $j$ has been optimistically unchoked.
%   \item[$n_j$] Among the $N_j$ unchokes, the number of times that peer $j$ responded with unchoke or supplied segments to peer $p$.
%   \item[$C_{r[j]}$] The cooperation ratio of peer $j$. If peer $j$ never supplied peer $p$, the information of $C_{r[j]}$ may not be available.
%   \item[$C_{r (max)}$] The maximum cooperation ratio of peer $p$’s neighbors, i.e., $C_{r (max)} = max(C_r)$.
% \end{description}
% 
% \begin{equation}
% \label{unchoke_gain}
%  G_j =
%   \begin{dcases}
%     \frac{n_j C_{r[j]}}{N_j} &\quad \text{if } n_j > 0\\
%     \frac{C_{r (max)}}{N_j + 1} &\quad \text{if } n_j = 0
%   \end{dcases}
% \end{equation}
% 
% Cursus $C_{r (max)}$ conubia nostra, per inceptos hymenaeos $j$ gadipiscing mollis massa $N_j = 0$, unc ut dui eget nulla venenatis aliquet $G_j = C_{r (max)}$.
% 
% Vestibulum accumsan eros nec magna. Vestibulum vitae dui. Vestibulum nec ligula et lorem consequat ullamcorper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Phasellus eget nisl ut elit porta ullamcorper. Maecenas tincidunt velit quis orci. Sed in dui. Nullam ut mauris eu mi mollis luctus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Sed cursus cursus velit. Sed a massa. 
% 
% Both \Cref{fig:tx_layer_4,fig:tx_layer_5} Phasellus eget nisl ut elit porta ``perfect'' tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra.
% 
% \begin{figure}[h]
% %\centering
%        \subfigure[Adaptation System Test 4]{\label{fig:tx_layer_4}\includegraphics[width=0.5\textwidth]{./Images/tx_layer_4}}  
%    %    \centering 
%        \subfigure[Adaptation System Test 5]{\label{fig:tx_layer_5}\includegraphics[width=0.5\textwidth]{./Images/tx_layer_5}}   
%         \caption{Adaptation System Behavior Test}
%         \label{fig:fig:adapt_behave_2}
% \end{figure}
% 
% Cras sed ante. Phasellus in massa. Curabitur dolor eros, gravida et, hendrerit ac, cursus non, massa. Aliquam lorem. In hac habitasse platea dictumst. Cras eu mauris. Quisque lacus. Donec ipsum. Nullam vitae sem at nunc pharetra ultricies. Vivamus elit eros, ullamcorper a, adipiscing sit amet, porttitor ut, nibh. Maecenas adipiscing mollis massa. Nunc ut dui eget nulla venenatis aliquet. Sed luctus posuere justo. Cras vehicula varius turpis. Vivamus eros metus, tristique sit amet, molestie dignissim, malesuada et, urna.